import { Component, OnInit } from '@angular/core';
import { Cliente, Usuario, Telefone } from 'src/dominio';
import { HttpClient } from '@angular/common/http';
import { Url } from 'src/app/utilidades/url';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-perfil-cliente',
  templateUrl: './perfil-cliente.component.html',
  styleUrls: ['./perfil-cliente.component.css']
})
export class PerfilClienteComponent implements OnInit {

  urlCliente: any
  cliente: any
  usuario: any
  operacao: string
  formCliente: FormGroup
  mensagemErro: string

  constructor(private http: HttpClient, private fb: FormBuilder) { }

  ngOnInit() {
    this.urlCliente = new Url().url.cliente
    this.verificaUsuarioAutenticado()

    this.formCliente = this.fb.group({
      id: [''],
      nome: ['', Validators.required],
      genero: ['Selecione...', Validators.required],
      dataNascimento: ['', Validators.required],
      cpf: ['', Validators.required],
      email: ['', Validators.required],
      senha: ['', Validators.required],
      confirmarSenha: ['', Validators.required],
      numeroTelefone: ['', Validators.required],
      idTelefone: [''],
      operacao: ['']
    });

  }

  verificaUsuarioAutenticado() {
    let user = JSON.parse(localStorage.getItem('currentUser'))
    console.log(user[0].id)
    this.findClienteByIdUsuario(user[0].id)
  }

  findClienteByIdUsuario(id) {
    console.log(id)
    this.http.post<Cliente>(this.urlCliente, {
      operacao:"consultar",
      usuario: {id: id}
    }).subscribe(
      (response) => {
        this.cliente = response
        this.retornaCliente(response)
        console.log(response)
      },
      (error) => { 
        console.log(error)
      }
    )
  }

  retornaCliente(cliente) {
    this.cliente = cliente
    let cliente2 = cliente
    console.log(cliente2)
    this.editar(cliente2)
  }

  editar(cliente: Cliente) {
    console.log(cliente)
    this.operacao = "alterar"
    this.formCliente.get('id').setValue(cliente[0].id)
    this.formCliente.get('nome').setValue(cliente[0].nome)
    this.formCliente.get('genero').setValue(cliente[0].genero)
    this.formCliente.get('dataNascimento').setValue(this.formatDate(cliente[0].dataNascimento))
    this.formCliente.get('cpf').setValue(cliente[0].cpf)
    this.formCliente.get('email').setValue(cliente[0].email)
    this.formCliente.get('senha').setValue(cliente[0].senha)
    this.formCliente.get('confirmarSenha').setValue(cliente[0].senha)

    this.formCliente.get('numeroTelefone').setValue(cliente[0].telefones[0].numero)
    this.formCliente.get('idTelefone').setValue(cliente[0].telefones[0].id)

  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

  onSubmit(form: FormGroup) {
    this.mensagemErro = undefined

    if(form.valid != true) {
      this.mensagemErro = "Algum dos campos não foi preenchido, preencha todos os campos para concluir o cadastro"
      console.log(form)
    }
      
    else {
      const cliente = new Cliente
      cliente.id = this.operacao == 'salvar' ? null : form.value.id
      cliente.nome = form.value.nome,
      cliente.genero = form.value.genero,
      cliente.dataNascimento = form.value.dataNascimento,
      cliente.cpf = form.value.cpf,
      cliente.email = form.value.email,
      cliente.senha = form.value.senha,
      cliente.confirmarSenha = form.value.confirmarSenha
      
      const telefone = new Telefone
      telefone.numero = form.value.numeroTelefone
      telefone.id = this.operacao == 'salvar' ? null : form.value.idTelefone

      console.log(telefone)
      cliente.telefones = []
      cliente.telefones[0] = telefone

      cliente.status = "ATIVO"

      console.log(cliente)
     
      // request
      this.http.post(this.urlCliente, {
        id: cliente.id,
        nome: cliente.nome,
        genero: cliente.genero,
        dataNascimento: cliente.dataNascimento,
        cpf: cliente.cpf,
        email: cliente.email,
        senha: cliente.senha,
        status: cliente.status,
        confirmarSenha: cliente.confirmarSenha,
        telefones: cliente.telefones,
        operacao : this.operacao
      }).subscribe(
        (response) => {
          console.log(response)
          //this.buscarClientes()
          this.formCliente.reset()
        },
        (error) => {
          console.log(error.error.text)
          this.mensagemErro = error.error.text;
        }
      )
    }
  }

}
